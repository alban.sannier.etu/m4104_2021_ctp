package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    String salle;
    String poste;
    String DISTANCIEL;
    // TODO Q2.c
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        DISTANCIEL = getResources().getStringArray(R.array.list_questions)[0];
        poste = "";
        salle = DISTANCIEL;
        // TODO Q2.c
        model = new SuiviViewModel(getActivity().getApplication());
        // TODO Q4
        Spinner spSalle = this.getView().findViewById(R.id.spSalle);
        Spinner spPoste = this.getView().findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);

        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spPoste.setAdapter(adapterPoste);
        spSalle.setAdapter(adapterSalle);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText editText = (EditText)view.findViewById(R.id.tvLogin);
            model.setUsername(editText.getText().toString());

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    // TODO Q9
}